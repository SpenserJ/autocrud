# AutoCRUD - Server Setup

<!-- AUTO-GENERATED-CONTENT:START (TOC) -->

<!-- AUTO-GENERATED-CONTENT:END -->

1. Merge AutoCRUD's `typeDefs` into your own:

    ```js
    import { makeExecutableSchema } from 'apollo-server-express';
    import * as AutoCRUD from '@vizworx/autocrud';

    import typeDefs from './typeDefs';

    const schema = makeExecutableSchema({
      typeDefs: [
        ...AutoCRUD.typeDefs,
        typeDefs,
      ],
    });
    ```

2. Apply AutoCRUD's directives to your schema, and get the context handler:

    ```js
    const { getContext: getAutoCRUDContext } = AutoCRUD.initialize(schema);
    ```

3. Add AutoCRUD's context to yours:

    ```js
    ...

    return new ApolloServer({
      schema,
      context: () => ({
        ...getAutoCRUDContext({
          knex,
          pubsub,
          pubsubMetadata,
          hooks,
        }),
      }),
    });
    ```

AutoCRUD relies on `getContext` internally, so it _must_ be called and spread into the larger context for each request. `getContext` returns an object with a single key, `AutoCRUD`, avoiding any namespacing issues with the broader app.

| Arg | Type | Default | Description |
|-----|------|---------|-------------|
| knex | Knex | **required** | An instance of Knex to use for database operations |
| pubsub | PubSub | none | See [Event Reporting](./event-reporting.md) |
| pubsubMetadata | any | none | Additional information to include with events
| hooks | Object of `fn`, `[fn]` | none | See [Hooks](./hooks.md)
