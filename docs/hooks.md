# AutoCRUD - Hooks

<!-- AUTO-GENERATED-CONTENT:START (TOC) -->
- [Summary](#summary)
- [Example](#example)
- [Considerations](#considerations)
- [Arguments](#arguments)
  * [`hookArgs`](#hookargs)
  * [`resolverArgs`](#resolverargs)
- [Hook Points](#hook-points)
<!-- AUTO-GENERATED-CONTENT:END -->

## Summary

AutoCRUD provides a set of hook points for callbacks to be run before, during, and after operations. Hooks can be given at a global level, or scoped by schema type.

Uses for hooks include:
- Modifying the GraphQL `args` before they're used in a query or mutation, such as deriving value based on something in the GraphQL context.
- Validating input in cases that the database schema cannot, such as checking for circular relationships (`Object 1 is owned by Object 2 is owned by Object 1`) or limiting the length of relationship chains in a given table to avoid prohibitively-expensive queries.
- Triggering other updates based on the current changes, such as updating a generated screenshot.

## Example

```js
const hooks = {
  preQuery: checkUserCanView,
  Book: {
    preCommit: [
      (hookArgs, resolverArgs) => ensureAuthorExists(resolverArgs.args.author),
      () => fetchBookByISBN.then((data) => addPublishingDataToBook(data)),
    ],
  },
  User: {
    preMutation: async ({ action, id }, { args, context }) => {
      if (action === 'create') {
        const nameIsTaken = await knex('users').where({ name: args.name }).first();

        if (nameIsTaken) {
          throw new Error(`The username ${args.name} already exists!`);
        }
      }

      args.derivedField = [args.name, args.email].join(',');
    },
  },
};

return new ApolloServer({
  schema,
  context: () => ({
    ...getAutoCRUDContext({
      knex,
      hooks,
    }),
  }),
});
```

## Considerations
- Any errors thrown in a hook will immediately cause the GraphQL request to fail, but _**only**_ errors in hook points specified below will cause database mutations to abort or roll back.
- Interface queries, for the moment, use a completely separate set of hooks from their child types; if hooks are being used on child types for concerns such as authorization, apply the same validation to the interface query as well.

## Arguments
Hooks are passed the same general arguments:
```js
const hook = (hookArgs, resolverArgs) => {
  const { type, table, ...additionalValues } = hookArgs
  const { obj, args, context, info } = resolverArgs
};
```

### `hookArgs`

The `hookArgs` object will always contain the schema type and database table of the current operation:

```js
{
  type: 'Book',
  table: 'books',
}
```

In addition, hook points may provide additional data specific to the operation being performed.

- 'all' query

  | Hook point | `hookArgs` |
  | - | - |
  | `preOperation` | |
  | `preQuery` | |
  | `postOperation` | `result` - The query response to be returned |
  | `postQuery` | `result` - The query response to be returned |

- Singular query

  | Hook point | `hookArgs` |
  | - | - |
  | `preOperation` | |
  | `preQuery` | |
  | `postOperation` | `result` - The query response to be returned |
  | `postQuery` | `result` - The query response to be returned |

- Interface query

  | Hook point | `hookArgs` |
  | - | - |
  | `preOperation` | `childTypes` - An array of schema types implementing this interface |
  | `preQuery` | `childTypes` - An array of schema types implementing this interface |
  | `postOperation` | `childTypes` - An array of schema types implementing this interface<br>`result` - The query response to be returned |
  | `postQuery` | `childTypes` - An array of schema types implementing this interface<br>`result` - The query response to be returned |

  _(The `table` field is not provided here, as interfaces have no related table)_

- Create

  | Hook point | `hookArgs` |
  | - | - |
  | `preOperation` | `action: 'create'`<br>`id` - A new UUID to be used when inserting the item into the database |
  | `preMutation` | `action: 'create'`<br>`id` |
  | `preCommit` | `action: 'create'`<br>`id`<br>`transaction` - The current Knex transaction |
  | `postCommit` | `action: 'create'`<br>`id`<br>`transaction` |
  | `prePublish` | `action: 'create'`<br>`id` |
  | `postPublish` | `action: 'create'`<br>`id` |
  | `postOperation` | `action: 'create'`<br>`id` |
  | `postMutation` | `action: 'create'`<br>`id` |

- Update

  | Hook point | `hookArgs` |
  | - | - |
  | `preMutation` | `action: 'update'` |
  | `preCommit` | `action: 'update'`<br>`prevState` - The database row prior to the mutation<br>`transaction` - The current Knex transaction |
  | `postCommit` | `action: 'update'`<br>`prevState`<br>`transaction` |
  | `prePublish` | `action: 'update'` |
  | `postPublish` | `action: 'update'` |
  | `postOperation` | `action: 'update'` |
  | `postMutation` | `action: 'update'` |

- Delete

  | Hook point | `hookArgs` |
  | - | - |
  | `preMutation` | `action: 'delete'` |
  | `preCommit` | `action: 'delete'`<br>`prevState` - The database row prior to the mutation<br>`transaction` - The current Knex transaction |
  | `postCommit` | `action: 'update'`<br>`prevState`<br>`transaction` |
  | `prePublish` | `action: 'delete'` |
  | `postPublish` | `action: 'delete'` |
  | `postOperation` | `action: 'delete'` |
  | `postMutation` | `action: 'delete'` |

### `resolverArgs`
The [standard set of arguments](https://graphql.org/learn/execution/#root-fields-resolvers) that were passed to the current query/mutation. `resolverArgs.args`, specifically, contains any arguments that were given and can be modified to affect the resulting database operation.

## Hook Points

- `preOperation`, `preQuery`, `preMutation`

  | Can modify `args` | Can roll back |
  | - | - |
  | ✔ | ✔ |

  This set of hooks are fired immediately, before the operation's resolver does anything else. `preOperation` will fire on both queries and mutations.

- `preCommit`

  | Can modify `args` | Can roll back |
  | - | - |
  | ✔ | ✔ |

  This hook is fired immediately prior to performing database mutations.

- `postCommit`

  | Can modify `args` | Can roll back |
  | - | - |
  | ✗ | ✔ |

  This hook is fired immediately after performing database mutations, but as part of the same database transaction.

- `prePublish`, `postPublish`

  | Can modify `args` | Can roll back |
  | - | - |
  | ✗ | ✗ |

  These hooks are fired before and after emitting a PubSub event, but _only_ if AutoCRUD has been given a `PubSub` instance to use.

- `postOperation`, `postQuery`, `postMutation`

  | Can modify `args` | Can roll back |
  | - | - |
  | ✗ | ✗ |

  These hooks are fired immediately before the resolver returns. `postOperation` will fire on both queries and mutations.
