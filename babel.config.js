module.exports = {
  presets: [[
    require.resolve('@vizworx/babel-preset'),
    {
      '@babel/preset-env': {
        targets: { node: '10' },
      },
    },
  ]],
};
