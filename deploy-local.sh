#!/bin/bash
# Builds the library and links it via the same runtime the script was called with

# We have to delete node_modules before running to ensure yarn can install over
# a previous npm link
rm -rf node_modules

yarn || exit $?
yarn run build || exit $?

# shellcheck disable=SC2154
if [[ $npm_execpath == *"yarn"* ]]; then
  echo -e "\n\nLinking to Yarn...\n"
  yarn workspaces focus --production || exit $?
  echo -e "\n\nTo use the package in your project, please run:\n  \`yarn link $(pwd)\`"
else
  # npm doesn't delete the dev packages when we run --only=prod
  rm -rf node_modules

  echo -e "\n\nLinking to NPM...\n"
  npm link --only=prod || exit $?
  rm ./package-lock.json
  echo -e "\n\nTo use the package in your project, please run:\n  \`npm link @vizworx/autocrud\`"
fi
