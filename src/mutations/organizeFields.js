// eslint-disable-next-line max-len
/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_mutationInfo", "_dbColumn"] }] */

import { getNamedType, isLeafType, GraphQLID } from 'graphql';
import { mapFieldToDBColumn } from '../utilities';
import {
  MissingQueriesDirective,
  MissingRelationDirective,
  MissingPivotInputType,
  UnexpectedIDsOnPivot,
  PivotInputMismatchedFields,
  UnknownRelationType,
} from '../errors';

/* eslint-disable no-param-reassign */
const accTableFields = (acc, table, id, key, value) => {
  if (!acc.base[table]) { acc.base[table] = {}; }

  if (!acc.base[table][id]) { acc.base[table][id] = {}; }

  acc.base[table][id][key] = value;
  return acc;
};

const accRelationship = (acc, table, values) => {
  acc.upsert[table] = (acc.upsert[table] || []).concat(values);
  return acc;
};
/* eslint-enable no-param-reassign */

const organizeFields = (schema, field, inputType, data, id, t) => Object.entries(data)
  .reduce((acc, [k, v]) => {
    const forField = inputType.getFields()[k]?._mutationInfo?.forField;

    // Field is defined as a scalar, or it's not included in the schema so we
    // can treat it like one.
    if (!forField || isLeafType(getNamedType(forField.type))) {
      const dbKey = mapFieldToDBColumn(field, k);

      return accTableFields(acc, field._queryInfo.table, id, dbKey, v);
    }

    const { _relationInfo } = forField;

    if (!_relationInfo) {
      throw new MissingRelationDirective(field, forField);
    }

    // One-to-____ relationship coming from this record (tag.childOf)
    if (_relationInfo.from) {
      const relationFromField = field.getFields()[_relationInfo.from];
      const dbKey = mapFieldToDBColumn(relationFromField, _relationInfo.from);

      return accTableFields(acc, field._queryInfo.table, id, dbKey, v);
    }

    // Many-to-many with pivot type
    const { pivotInput: pivotInputName } = getNamedType(forField.type)?._queryInfo ?? {};

    if (pivotInputName) {
      const pivotInput = schema.getType(pivotInputName);

      if (!pivotInput) { throw new MissingPivotInputType(pivotInputName); }

      // Go through all of the fields on the resulting type and copy the
      // ones that match, then add in the _relationInfo.to, and try to map the remaining ID
      const { table } = getNamedType(forField.type)._queryInfo;
      const pivotFields = Object.values(getNamedType(forField.type).getFields());

      // Determine which ID columns are used in the pivot
      if (!acc.pivotKeys[table]) {
        const remainingIDs = pivotFields.filter((pivotField) => {
          if (getNamedType(pivotField.type) !== GraphQLID) { return false; }

          if (pivotField.name === _relationInfo.to) { return false; }

          return true;
        });

        if (remainingIDs.length !== 1) {
          throw new UnexpectedIDsOnPivot(
            forField.type,
            // Put the key that we filtered out back on
            remainingIDs.map(({ name }) => name).concat(_relationInfo.to),
          );
        }

        acc.pivotKeys[table] = {
          near: mapFieldToDBColumn(forField.type, _relationInfo.to),
          far: mapFieldToDBColumn(forField.type, remainingIDs[0].name),
        };
      }

      const pivotRows = v.map((row) => {
        const rowValue = {};
        pivotFields.forEach((pivotField) => {
          if (typeof row[pivotField.name] !== 'undefined') {
            rowValue[pivotField.name] = row[pivotField.name];
          }
        });

        const { near, far } = acc.pivotKeys[table];
        rowValue[near] = id;
        rowValue[far] = row.id;
        const remainingValues = Object.keys(row)
          .filter((key) => (key !== 'id' && typeof rowValue[key] === 'undefined'));

        if (remainingValues.length !== 0) {
          throw new PivotInputMismatchedFields(
            pivotInput,
            getNamedType(forField.type),
            remainingValues,
          );
        }

        return rowValue;
      });

      return accRelationship(acc, table, pivotRows);
    }

    // Many-to-many without pivot type
    if (_relationInfo.through) {
      // Near and Far are DB columns, so we don't need to mapFieldToDBColumn
      const { near, far, table } = _relationInfo.through;
      acc.pivotKeys[table] = { near, far };
      const pivotRows = v.map((farID) => ({ [near]: id, [far]: farID }));

      return accRelationship(acc, table, pivotRows);
    }

    // One-to-many relationship going to other record (tag.parentOf)
    if (_relationInfo.to) {
      const targetType = getNamedType(forField.type);
      const targetTable = targetType._queryInfo?.table;

      if (!targetTable) {
        throw new MissingQueriesDirective(getNamedType(forField.type));
      }

      const dbKey = mapFieldToDBColumn(targetType, _relationInfo.to);

      // For each ID, flag that we need to update the far record with this record's ID
      v.forEach((farID) => accTableFields(acc, targetTable, farID, dbKey, id));

      // For the items that are currently assigned, we need to remove the relationship
      const currentFarQuery = t(targetTable)
        .select('id')
        .where(dbKey, id)
        .whereNotIn('id', v)
        .then((currentFar) => currentFar
          .forEach((far) => accTableFields(acc, targetTable, far.id, dbKey, null)));

      acc.promises.push(currentFarQuery);

      return acc;
    }

    // TODO: This doesn't support one-to-one relationships going "to"
    // another record. It only supports one-to-many going to. This is
    // because we don't currently have any one-to-one relationships
    // to test this with.
    throw new UnknownRelationType(field, forField);
  }, { base: {}, upsert: {}, pivotKeys: {}, promises: [] });

export default organizeFields;
