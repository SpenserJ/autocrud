import {
  FieldParsingError,
  UnexpectedOperationDuringUpdate,
  MissingPivotKey,
} from '../errors';

// Take have=[{}] and want={id:{}}, and determine { insert: [], update: {}, delete: {} }
// keepColumns is used to retain values that haven't changed, and is used for pivots
const getChangedRows = (have, want, idColumn = 'id', keepColumns = []) => {
  const changed = { update: [], delete: [] };
  have.forEach((haveRow) => {
    // Handle deleted rows
    const wantRow = want[haveRow[idColumn]];

    if (!wantRow) { changed.delete.push(haveRow); return; }

    // Determine what fields have changed in this row
    const changedInRow = Object.entries(wantRow).filter(([key, value]) => {
      if (typeof haveRow[key] === 'undefined') {
        throw new FieldParsingError(key);
      }

      // If we're working with booleans, typecast it to a bool for comparison
      if (typeof value === 'boolean') { return !!haveRow[key] !== value; }

      return haveRow[key] !== value;
    });

    if (changedInRow.length > 0) {
      // Generate the change object for the row, including any unchanged columns
      // that we want to retain, such as both IDs on a WeightedID pivot
      const finalRow = Object.fromEntries(changedInRow);
      keepColumns.forEach((column) => {
        if (typeof finalRow[column] !== 'undefined') { return; }

        finalRow[column] = haveRow[column];
      });

      changed.update.push([haveRow[idColumn], finalRow]);
    }
  });

  // Calculate the rows to be inserted
  changed.insert = Object.entries(want)
    .filter(([id]) => !have.find((test) => test[idColumn] === id))
    .map(([, value]) => value);

  return changed;
};

// Return the aggregated changes, or false if nothing happened
// { insert: { Tag: [...] }, update: {}, delete: {} }
export const aggregateChanges = (...args) => {
  // Track if anything has actually changed
  let changed = false;

  const aggregated = args.flat().reduce((acc, next) => {
    Object.entries(next).forEach(([table, changes]) => {
      if (changes.length === 0) { return; }

      Object.entries(changes).forEach(([operation, values]) => {
        if (values.length === 0) { return; }

        changed = true;
        values.forEach((cur) => {
          const [id, rest] = (operation === 'update')
            ? cur // [ [ 'ck0zfkufq01n70162ilyxomog', { name: 'Heritage Park 9' } ] ]
            : [undefined, cur]; // [ { aID: '...', bID: '...' }, { aID: '...', bID: '...' } ]

          acc.push({
            operation,
            table,
            id: (operation === 'insert' && rest.id) ? rest.id : id,
            values: rest,
          });
        });
      });
    });

    return acc;
  }, []);

  return changed ? aggregated : false;
};

export const knexUpdateBaseModels = (t, fields) => Object.entries(fields.base)
  .map(([table, rows]) => {
    // Fetch the rows that we want to update, so we can determine the changes
    const ids = Object.keys(rows);

    return t(table).forShare().whereIn('id', ids).then((old) => {
      const changed = getChangedRows(old, rows);

      // Throw an error if rows need to be inserted or deleted, since that should
      // never be done when updating a base model
      if (changed.insert.length !== 0) {
        throw new UnexpectedOperationDuringUpdate('insert', changed.insert);
      }

      if (changed.delete.length !== 0) {
        throw new UnexpectedOperationDuringUpdate('delete', changed.delete);
      }

      // If there are no rows that need to be updated, return early
      if (changed.update.length === 0) { return {}; }

      // Apply the updates to each of the rows
      // TODO: Maybe check to confirm the correct number of rows were affected?
      return (!changed.update.length)
        ? {}
        : Promise.all(changed.update.map(([id, values]) => t(table).update(values).where('id', id)))
          .then(() => ({ [table]: changed }));
    });
  });

export const knexUpdatePivots = (t, fields, id) => Object.entries(fields.upsert)
  .flatMap(([table, wantRows]) => {
    // Ensure all of the pivots have the same keys and generate an array
    // of IDs that shouldn't be deleted
    const expected = wantRows.length !== 0 ? Object.keys(wantRows[0]) : [];
    const wantRowsKeyed = wantRows.reduce((acc, next) => {
      expected.forEach((key) => {
        if (typeof next[key] === 'undefined') {
          throw new MissingPivotKey(table, key);
        }
      });

      acc[next[fields.pivotKeys[table].far]] = next;
      return acc;
    }, {});

    // Fetch the rows that we want to update, so we can determine the changes
    return t(table).forShare()
      .whereIn(fields.pivotKeys[table].near, [id])
      .then((haveRows) => {
        const changed = getChangedRows(
          haveRows,
          wantRowsKeyed,
          fields.pivotKeys[table].far,
          [fields.pivotKeys[table].near, fields.pivotKeys[table].far],
        );

        const promises = [];

        // Pivots are upserted, so combine inserts and updates
        if (changed.insert.length) {
          promises.push(t(table).insert(changed.insert));
        }

        changed.update.forEach(([, upsertRow]) => {
          const { near, far } = fields.pivotKeys[table];
          promises.push(t(table)
            .update(upsertRow)
            .where(near, upsertRow[near])
            .where(far, upsertRow[far]));
        });

        // Only apply deletes if there are rows, or we'll generate a broken query
        if (changed.delete.length !== 0) {
          const deleteIDs = changed.delete.map((v) => v[fields.pivotKeys[table].far]);
          promises.push(t(table).delete()
            .where(fields.pivotKeys[table].near, id)
            .whereIn(fields.pivotKeys[table].far, deleteIDs));
        }

        return Promise.all(promises)
          // TODO: Maybe check to confirm the correct number of rows were affected?
          .then(() => ({ [table]: changed }));
      });
  });
