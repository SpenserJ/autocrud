/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_filterInfo"] }] */

import { getNamedType, GraphQLID, GraphQLInt } from 'graphql';
import { GraphQLUnsupportedFilterOperation } from '../errors';
import {
  getRelationInfo,
  recurseToRelationQueries,
  mapFieldToDBColumn,
} from '../utilities';

/**
 * Adds a set of WHERE filters to a Knex query object.
 *
 * Usage:
 * ```js
 * const resolver = (obj, args) => {
 *   const builder = new FilterBuilder(knex, schema);
 *
 *   const queryWithFilters = builder.apply(
 *     knex.select('*').from(obj._queryInfo.table),
 *     args.filter,
 *     returnTypeName,
 *   );
 * ```
 */
class FilterBuilder {
  constructor(knex, schema) {
    this.knex = knex;
    this.schema = schema;
    this.relationPaths = [];
    this.relationQueries = {};
  }

  apply = (
    rawQuery,
    filters,
    typeName,
  ) => {
    this.recurseCollectRelationPaths(typeName, filters);
    this.relationPaths.forEach((p) => recurseToRelationQueries(this.knex, this.relationQueries, p));

    return this.applyKnexFilters({
      query: rawQuery,
      filters,
      typeName,
    });
  };

  generateNestedFilter = ({
    queryAlias,
    relation,
    value,
    shouldInclude = true,
  }) => {
    if (!this.relationQueries[queryAlias]) {
      throw new Error(`Query alias '${queryAlias}' does not have a relation query`);
    }

    const allSubquery = this.relationQueries[queryAlias].map(([tableAlias, subquery]) => {
      const select = relation.through
        ? `${tableAlias}_junction.${relation.through.near}`
        : `${tableAlias}.${relation.to}`;

      /*
        Knex methods mutate the builder. Since a recursive filter like:

        OR:[
          {childOf:{ name: "parent"}},
          {childOf:{childOf:{ name: "parent"}}}
        ]

        will end up using the first branch's subquery again while building the
        second, we have to make sure we're using independent copies to avoid
        having subsequent operations on each branch show up on both.
      */
      const base = subquery.clone().select(select);

      const where = (shouldInclude ? base.where : base.whereNot).bind(base);

      // Rule is disabled because these are intentionally mutally recursive
      // eslint-disable-next-line no-use-before-define
      return where((builder) => this.applyKnexFilters({
        query: builder,
        filters: value,
        typeName: relation.targetModel.name,
        prevQueryAlias: queryAlias,
      }));
    });

    return allSubquery[0].union(allSubquery.slice(1));
  }

  applyKnexFilters = ({
    query,
    filters,
    typeName,
    prevQueryAlias = 'base',
    whereOr = false,
  }) => {
    const modelType = this.schema.getType(typeName);
    const filterType = this.schema.getType(`${typeName}Filter`);

    return [].concat(filters)
      .flatMap((arg) => Object.entries(arg))
      .reduce((acc, [key, rawValue]) => {
        const filterProperty = filterType.getFields()[key];
        const { property } = filterProperty._filterInfo;
        const modelProperty = (property === 'AND' || property === 'OR')
          ? null
          : modelType.getFields()[property];

        const modelPropertyType = modelProperty ? getNamedType(modelProperty.type) : null;
        const where = (whereOr ? acc.orWhere : acc.where).bind(acc);
        const queryAlias = (property === 'AND' || property === 'OR')
          ? prevQueryAlias
          : `${prevQueryAlias}_${property}`;

        let value = rawValue;

        if (filterProperty.type.name === 'String') {
          // TODO: String operations needs to support newlines and markdown in some cases
          value = value.split(' ').join('%');
        }

        // Map the GraphQL field to the DB column
        const column = mapFieldToDBColumn(modelProperty, property);

        switch (filterProperty._filterInfo.operation) {
          case 'startsWith': return where(column, 'like', `${value}%`);
          case 'endsWith': return where(column, 'like', `%${value}`);
          case 'contains': return where(column, 'like', `%${value}%`);
          case 'equals':
            if (modelPropertyType === GraphQLID) { return where(column, value); }

            if (modelPropertyType === GraphQLInt) { return where(column, value); }

            return where(column, 'like', value);
          case 'lt': return where(column, '<', value);
          case 'lte': return where(column, '<=', value);
          case 'gt': return where(column, '>', value);
          case 'gte': return where(column, '>=', value);
          case 'in': return where(column, 'in', value);
          case 'or': return where((builder) => this.applyKnexFilters({
            query: builder,
            filters: value,
            typeName,
            prevQueryAlias: queryAlias,
            whereOr: true,
          }));
          case 'and': return where((builder) => this.applyKnexFilters({
            query: builder,
            filters: value,
            typeName,
            prevQueryAlias: queryAlias,
          }));
          // TODO: This shouldn't mimic some, but we need to write a query for
          // joining the relationship without causing column name collisions
          case 'relation':
          case 'some': {
            const relation = getRelationInfo(modelProperty);
            const inRows = this.generateNestedFilter({
              queryAlias,
              relation,
              value,
            });

            return where(relation.from, 'in', inRows);
          }

          case 'none': {
            const relation = getRelationInfo(modelProperty);
            const inRows = this.generateNestedFilter({
              queryAlias,
              relation,
              value,
            });

            return where(relation.from, 'not in', inRows);
          }

          case 'every': {
            const relation = getRelationInfo(modelProperty);
            const inRows = this.generateNestedFilter({
              queryAlias,
              relation,
              value,
              shouldInclude: false,
            });

            // TODO: Not sure if this is going to work properly. The subquery
            // selects relations that don't match, and then gets everything
            // except for those.
            // If there are no relations on something, should it still match?
            return where(relation.from, 'not in', inRows);
          }

          default: throw new GraphQLUnsupportedFilterOperation(
            typeName,
            property,
            filterProperty._filterInfo.operation,
          );
        }
      }, query);
  };

  recurseCollectRelationPaths = (typeName, filters, path = []) => {
    const modelType = this.schema.getType(typeName);
    const filterType = this.schema.getType(`${typeName}Filter`);

    Object.entries(filters).forEach(([key, rawValue]) => {
      const filterProperty = filterType.getFields()[key];
      const { property, operation } = filterProperty._filterInfo;

      // We need to branch here
      if (operation === 'and' || operation === 'or') {
        return rawValue.forEach((v) => this.recurseCollectRelationPaths(typeName, v, path));
      }

      const modelProperty = modelType.getFields()[property];
      const relation = getRelationInfo(modelProperty, false);

      // We've reached the end of this chain
      if (!relation) {
        return this.relationPaths.push(path);
      }

      const obj = {
        name: property,
        onType: modelType,
      };

      return this.recurseCollectRelationPaths(relation.targetModel.name, rawValue, [...path, obj]);
    });
  };
}

export default FilterBuilder;
