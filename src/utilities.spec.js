import { gql, makeExecutableSchema } from 'apollo-server-express';
import { isScalarField } from './utilities';

describe('utilities', () => {
  describe('isScalarField', () => {
    let schema;

    beforeAll(() => {
      const typeDefs = gql`
        type Person {
          id: ID!
          name: String
          birthdate: DateTime!
          residence: Place
          isCitizen: Boolean!
        }

        type Place {
          id: ID!
          lat: Float!
          long: Float!
          name: Float
          population: Int!
          residents: [Person!]!
          things: [Thing!]!
        }

        type Thing {
          id: ID!
          typeOfThing: String!
          description: String
          owner: Person
          location: Place!
          metadata: JSON
        }

        scalar DateTime
        scalar JSON
      `;

      schema = makeExecutableSchema({ typeDefs });
    });

    test.each([
      ['Person.id', true],
      ['Place.population', true],
      ['Place.lat', true],
      ['Person.name', true],
      ['Person.isCitizen', true],
      ['Person.birthdate', true],
      ['Thing.metadata', true],
      ['Thing.owner', false],
      ['Person.residence', false],
      ['Place.things', false],
    ])('%s: %s', (path, expected) => {
      const [type, field] = path.split('.');
      // eslint-disable-next-line no-underscore-dangle
      const fields = schema.getTypeMap()[type]._fields;

      expect(isScalarField(fields[field])).toEqual(expected);
    });
  });
});
