/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_filterInfo"] }] */

import { recurseToRelationQueries } from '../utilities';

const applyOrderLimit = (query, orderBy, desc) => query
  .orderBy(orderBy, desc ? 'DESC' : 'ASC')
  .limit(1);

const recurseToNestedQueries = (
  relationQueries,
  orderByName,
  desc,
  paths,
  prevAlias = 'base',
) => {
  // TODO: This may need to group parent relations together
  const [{ name: fieldName }, ...remaining] = paths;
  const queryAlias = `${prevAlias}_${fieldName}`;
  const relationSubquery = relationQueries[queryAlias];

  if (!relationSubquery) {
    return { [orderByName]: `${prevAlias}.${fieldName}` };
  }

  const allSubqueryWithSelect = relationSubquery.map(([tableAlias, subquery]) => {
    const selectBy = recurseToNestedQueries(
      relationQueries,
      orderByName,
      desc,
      remaining,
      tableAlias,
    );

    return subquery.select(selectBy);
  });

  return {
    [orderByName]: applyOrderLimit(
      allSubqueryWithSelect[0].union(allSubqueryWithSelect.slice(1)),
      orderByName,
      desc,
    ),
  };
};

const applyKnexOrderBy = (knex, schema, rawQuery, orderBy) => {
  // Begin by clustering the orderBy fields into their various (nested) tables
  const relationQueries = orderBy.reduce((acc, next) => recurseToRelationQueries(
    knex,
    acc,
    next.field.fieldPath,
  ), {});

  // Uncomment this to assist with understanding the relation queries that will be nested
  /*
  console.log(Object.fromEntries(
    Object.entries(relationQueries).map(([queryAlias, queries]) => {
      const sqlQueries = Object.fromEntries(queries
        .map(([tableAlias, query]) => ([tableAlias, query.toString()])));

      return [queryAlias, sqlQueries];
    }),
  ));
  */

  const orderSelectFields = orderBy.reduce((acc, { field: { fieldPath, name }, desc }) => ({
    ...acc,
    ...recurseToNestedQueries(relationQueries, name, desc, fieldPath),
  }), {});

  const orderedQuery = rawQuery
    .select(orderSelectFields)
    .orderByRaw(orderBy.map(({ field, desc }) => {
      const order = desc ? 'DESC NULLS LAST' : 'ASC NULLS FIRST';

      return knex.raw(`? ${order}`, [knex.ref(field.name)]);
    }).join(', '));

  return orderedQuery;
};

export default applyKnexOrderBy;
