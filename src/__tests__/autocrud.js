import { gql } from 'apollo-server-express';

import testGraphql from '../testUtils/expect-graphql';
import AutoCRUDTester from '../testUtils/AutoCRUDTester';
import { MissingIDField } from '../errors';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';

const typeDefs = gql`
  type Vignette @autocrud(table: "vignettes") {
    id: ID!
    name: String!
  }

  type Query
  type Mutation
`;

const resolvers = {
  Query: {},
  Mutation: {},
};

const getTester = () => new AutoCRUDTester({
  typeDefs: [
    autoCRUDTypeDefs,
    typeDefs,
  ],
  resolvers,
  directives: { ...AutoCRUDDirectiveStages },
});

describe('AutoCRUDDirective', () => {
  describe('invalid usage', () => {
    test('missing id field on basic type should throw', () => {
      expect.assertions(1);
      const schemaThrows = Promise.resolve().then(() => new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          gql`
            type Vignette @autocrud(table: "vignettes") {
              name: String!
            }
          `,
        ],
        directives: { ...AutoCRUDDirectiveStages },
      }));

      expect(schemaThrows).rejects.toThrow(MissingIDField);
    });

    test('missing id field on pivot type should not throw', async () => {
      // We don't expect an error, and Jest can be a bit weird about using
      // `expect.assertions` to confirm that an async/promise doesn't throw. If
      // this throws, the test will fail
      await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          gql`
            type Vignette @autocrud(table: "vignettes", pivotInput: "PivotWithMetadata") {
              vignetteID: ID!
              storyID: ID!
            }
          `,
        ],
        directives: { ...AutoCRUDDirectiveStages },
      });
    });
  });

  describe('default values', () => {
    describe('schema', () => {
      let tester;
      beforeAll(async () => {
        tester = await getTester();
        await tester.resetDB();
      });

      testGraphql.schema(() => tester.schema)
        .shouldHaveTypes([
          'Vignette',
        ])
        .shouldHaveInputs([
          'VignetteParams',
          'VignetteFilter',
        ])
        .shouldHaveQueries({
          vignette: ['Vignette', { id: 'ID!' }],
          allVignettes: ['[Vignette!]!', { filter: 'VignetteFilter', orderBy: '[VignetteOrderBy]' }],
        })
        .shouldHaveMutations({
          createVignette: ['Vignette!', { vignette: 'VignetteParams!' }],
          updateVignette: ['Vignette!', { id: 'ID!', vignette: 'VignetteParams!' }],
          deleteVignette: ['Boolean', { id: 'ID!' }],
        });

      testGraphql.object(() => tester.schema.getType('VignetteFilter'))
        .shouldOnlyHaveFields({
          AND: '[VignetteFilter!]',
          OR: '[VignetteFilter!]',
          id_in: '[ID!]',
          id: 'ID',
          name: 'String',
          name_contains: 'String',
          name_startsWith: 'String',
          name_endsWith: 'String',
          name_in: '[String!]',
        });
    });

    describe('resolvers', () => {
      let tester;

      beforeEach(async () => {
        tester = await getTester();
        await tester.resetDB();

        const { knex } = tester;

        await knex.schema.createTable('vignettes', (t) => {
          t.uuid('id').unique();
          t.string('name');
        });

        await knex('vignettes').insert([
          { id: tester.uuid('apples'), name: 'apples' },
          { id: tester.uuid('bananas'), name: 'bananas' },
          { id: tester.uuid('cherry'), name: 'cherry' },
        ]);
      });

      test('should resolve allVignettes', async () => {
        const graphqlResult = await tester.query(`
          query {
            allVignettes { id name }
          }
        `);

        expect(graphqlResult).graphqlResultNotToHaveErrors();
        expect(graphqlResult).toEqual({
          data: {
            allVignettes: [
              { id: tester.uuid('apples'), name: 'apples' },
              { id: tester.uuid('bananas'), name: 'bananas' },
              { id: tester.uuid('cherry'), name: 'cherry' },
            ],
          },
        });
      });

      test('should resolve vignette', async () => {
        const graphqlResult = await tester.query(`
          query($id: ID!) {
            vignette(id: $id) {
              name
            }
          }
        `, { id: tester.uuid('apples') });

        expect(graphqlResult).graphqlResultNotToHaveErrors();
        expect(graphqlResult).toEqual({
          data: {
            vignette: { name: 'apples' },
          },
        });
      });

      test('should resolve createVignette', async () => {
        const graphqlResult = await tester.query(`
          mutation($vignette: VignetteParams!) {
            createVignette(vignette: $vignette) {
              id
              name
            }
          }
        `, { vignette: { name: 'eggplant' } });

        expect(graphqlResult).graphqlResultNotToHaveErrors();
        expect(graphqlResult).toMatchObject({
          data: {
            createVignette: {
              id: expect.any(String),
              name: 'eggplant',
            },
          },
        });

        const result = await tester.knex('vignettes').where({ name: 'eggplant' });

        expect(result.length).toEqual(1);
      });

      test('should resolve updateVignette', async () => {
        const graphqlResult = await tester.query(`
          mutation($id: ID!, $vignette: VignetteParams!) {
            updateVignette(id: $id, vignette: $vignette) {
              id
              name
            }
          }
        `, { id: tester.uuid('apples'), vignette: { name: 'aaa' } });

        expect(graphqlResult).graphqlResultNotToHaveErrors();
        expect(graphqlResult).toEqual({
          data: {
            updateVignette: {
              id: tester.uuid('apples'),
              name: 'aaa',
            },
          },
        });

        const result = await tester.knex('vignettes')
          .where({ id: tester.uuid('apples') })
          .first();

        expect(result?.name).toEqual('aaa');
      });

      test('should resolve deleteVignette', async () => {
        const graphqlResult = await tester.query(`
          mutation($id: ID!) {
            deleteVignette(id: $id)
          }
        `, { id: tester.uuid('apples') });

        expect(graphqlResult).graphqlResultNotToHaveErrors();

        const result = await tester.knex('vignettes').where({ id: tester.uuid('apples') });

        expect(result?.length).toEqual(0);
      });

      test('should resolve filters', async () => {
        const graphqlResult = await tester.query(`
          query($filter: VignetteFilter) {
            allVignettes(filter: $filter) {
              id
              name
            }
          }
        `, { filter: { name_contains: 's' } });

        expect(graphqlResult).graphqlResultNotToHaveErrors();
        expect(graphqlResult).toMatchObject({
          data: {
            allVignettes: [
              { id: tester.uuid('apples'), name: 'apples' },
              { id: tester.uuid('bananas'), name: 'bananas' },
            ],
          },
        });
      });
    });
  });

  describe('custom values', () => {
    let tester;

    beforeEach(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          typeDefs,
          gql`
            type Asset @autocrud(
              table: "assets",
              readOne: false,
              readAll: false,
              create: false,
              update: false,
              delete: false,
            ) {
              id: ID!
              name: String!
            }
          `,
        ],
        resolvers,
        directives: {
          ...AutoCRUDDirectiveStages,
        },
      });
    });

    testGraphql.object(() => tester.schema.getQueryType())
      .shouldNotHaveFields(['allAssets', 'asset']);
  });

  describe('relations through', () => {
    let tester;
    beforeEach(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          relationTypeDefs,
          typeDefs,
          gql`
            type Story @autocrud(table: "stories") {
              id: ID!
              name: String!
              vignettes: [Vignette!]! @relation(through: { table: "vignettes_stories", near: "storyID", far: "vignetteID" })
            }
          `,
        ],
        resolvers,
        directives: {
          ...AutoCRUDDirectiveStages,
          ...RelationDirectiveStages,
        },
      });

      await tester.resetDB();
    });

    describe('relation filter fields', () => {
      testGraphql.schema(() => tester.schema)
        .shouldHaveInputs(['StoryFilter']);

      testGraphql.object(() => tester.schema.getType('StoryFilter'))
        .shouldHaveFields({
          vignettes_some: ['VignetteFilter'],
          vignettes_every: ['VignetteFilter'],
          vignettes_none: ['VignetteFilter'],
        });
    });

    test('should correctly add a pivot record', async () => {
      const { knex } = tester;

      await knex.schema.createTable('vignettes', (t) => {
        t.uuid('id').unique();
        t.string('name');
      });

      await knex.schema.createTable('stories', (t) => {
        t.uuid('id').unique();
        t.string('name');
      });

      await knex.schema.createTable('vignettes_stories', (t) => {
        t.uuid('vignetteID').references('vignettes.id');
        t.uuid('storyID').references('stories.id');
      });

      await knex('vignettes').insert([
        { id: tester.uuid('apples'), name: 'apples' },
        { id: tester.uuid('bananas'), name: 'bananas' },
      ]);

      const graphqlResult = await tester.query(`
        mutation ($story: StoryParams!) {
          createStory(story: $story) {
            name
            vignettes {
              name
            }
          }
        }
      `, {
        story: {
          name: 'Hi there!',
          vignetteIDs: [tester.uuid('apples')],
        },
      });

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          createStory: {
            name: 'Hi there!',
            vignettes: [{ name: 'apples' }],
          },
        },
      });
    });
  });

  describe('relations with a custom pivot type', () => {
    let tester;

    beforeEach(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          relationTypeDefs,
          gql`
            type Query
            type Mutation

            input PivotWithMetadata {
              id: ID!
              metadata: String!
            }

            type Vignette @autocrud(table: "vignettes") {
              id: ID!
              name: String!
            }

            type Story @autocrud(table: "stories") {
              id: ID!
              name: String!
              vignettes: [VignetteStory!]! @relation(to: "storyID")
            }

            type VignetteStory @autocrud(
              table: "vignettes_stories"
              pivotInput: "PivotWithMetadata"
              readOne: false,
              create: false,
              update: false,
              delete: false,
            ) {
              vignetteID: ID!
              vignette: Vignette! @relation(from: "vignetteID")
              storyID: ID!
              story: Story! @relation(from: "storyID")
              metadata: String!
            }
          `,
        ],
        resolvers,
        directives: {
          ...AutoCRUDDirectiveStages,
          ...RelationDirectiveStages,
        },
      });

      await tester.resetDB();
    });

    testGraphql.object(() => tester.schema.getType('StoryParams'))
      .shouldHaveFields({
        vignetteIDs: ['[PivotWithMetadata!]'],
      });

    test('should correctly add a pivot record', async () => {
      const { knex } = tester;

      await knex.schema.createTable('vignettes', (t) => {
        t.uuid('id').unique();
        t.string('name');
      });

      await knex.schema.createTable('stories', (t) => {
        t.uuid('id').unique();
        t.string('name');
      });

      await knex.schema.createTable('vignettes_stories', (t) => {
        t.uuid('vignetteID').references('vignettes.id');
        t.uuid('storyID').references('stories.id');
        t.string('metadata');
      });

      await knex('vignettes').insert([
        { id: tester.uuid('apples'), name: 'apples' },
        { id: tester.uuid('bananas'), name: 'bananas' },
      ]);

      const graphqlResult = await tester.query(`
        mutation ($story: StoryParams!) {
          createStory(story: $story) {
            name
            vignettes {
              metadata
              vignette {
                name
              }
            }
          }
        }
      `, {
        story: {
          name: 'Hi there!',
          vignetteIDs: [
            { id: tester.uuid('apples'), metadata: 'ten' },
            { id: tester.uuid('bananas'), metadata: 'eleven' },
          ],
        },
      });

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          createStory: {
            name: 'Hi there!',
            vignettes: [
              {
                metadata: 'ten',
                vignette: {
                  name: 'apples',
                },
              },
              {
                metadata: 'eleven',
                vignette: {
                  name: 'bananas',
                },
              },
            ],
          },
        },
      });
    });
  });

  describe('automatic generation of root types', () => {
    let tester;
    let rows;
    let caught;

    beforeEach(async () => {
      try {
        tester = await new AutoCRUDTester({
          typeDefs: [
            autoCRUDTypeDefs,
            gql`
              type Vignette @autocrud(table: "vignettes") {
                id: ID!
                name: String!
              }
            `,
          ],
          directives: {
            ...AutoCRUDDirectiveStages,
          },
        });
      } catch (err) {
        caught = err;
      }

      const { knex } = tester;
      await tester.resetDB();

      rows = [
        { id: tester.uuid('fenugreek'), name: 'fenugreek' },
        { id: tester.uuid('geranium'), name: 'geranium' },
      ];

      await knex.schema.createTable('vignettes', (t) => {
        t.uuid('id').unique();
        t.string('name');
      });

      await knex('vignettes').insert(rows);
    });

    test('shouldn\'t break if root types aren\'t passed in', () => {
      expect(caught).toEqual(undefined);
    });

    testGraphql.schema(() => tester.schema).shouldHaveTypes([
      'Query',
      'Mutation',
      'Vignette',
    ]);

    test('should resolve a query', async () => {
      const graphqlResult = await tester.query(`
        query {
          allVignettes { id name }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          allVignettes: rows,
        },
      });
    });

    test('should resolve a mutation', async () => {
      const graphqlResult = await tester.query(`
        mutation($id: ID!) {
          deleteVignette(id: $id)
        }
      `, { id: tester.uuid('fenugreek') });

      expect(graphqlResult).graphqlResultNotToHaveErrors();
    });
  });

  describe('excludeFromInput', () => {
    let tester;

    beforeEach(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          gql`
            type Book @autocrud(table: "books") {
              id: ID!
              title: String!
              lastSearched: String @excludeFromInput
            }
          `,
        ],
        directives: { ...AutoCRUDDirectiveStages },
      });
    });

    test('should exclude fields', () => {
      expect(tester.schema.getType('BookParams'))
        .not.graphqlObjectToHaveField('lastSearched');
    });
  });

  describe('unsupported GraphQL syntax', () => {
    describe('unions - no filtering or sorting', () => {
      let tester;

      beforeEach(async () => {
        tester = await new AutoCRUDTester({
          typeDefs: [
            autoCRUDTypeDefs,
            gql`
              type FruitPurchase @autocrud(table: "purchases") {
                id: ID!
                quantity: Int!
                pricePer: Float!
                fruit: Fruit
              }

              union Fruit = Apple | Banana | Cherry | Durian

              type Apple {
                variety: String!
              }

              type Banana {
                ripeness: Int!
              }

              type Cherry {
                hasPit: Boolean!
              }

              type Durian {
                sealed: Boolean!
              }

              type Query
              type Mutation
            `,
          ],
          resolvers: {
            ...resolvers,
            Fruit: { __resolveType: () => {} },
          },
          directives: { ...AutoCRUDDirectiveStages },
        });
      });

      describe('schema', () => {
        testGraphql.schema(() => tester.schema)
          .shouldHaveTypes([
            'FruitPurchase',
          ])
          .shouldHaveInputs([
            'FruitPurchaseFilter',
            'FruitPurchaseOrderBy',
          ]);

        testGraphql.object(() => tester.schema.getType('FruitPurchaseFilter'))
          .shouldOnlyHaveFields({
            AND: '[FruitPurchaseFilter!]',
            OR: '[FruitPurchaseFilter!]',
            id_in: '[ID!]',
            id: 'ID',
            quantity: 'Int',
            quantity_lt: 'Int',
            quantity_lte: 'Int',
            quantity_gt: 'Int',
            quantity_gte: 'Int',
            quantity_in: '[Int!]',
            pricePer: 'Float',
            pricePer_lt: 'Float',
            pricePer_lte: 'Float',
            pricePer_gt: 'Float',
            pricePer_gte: 'Float',
            pricePer_in: '[Float!]',
          });

        testGraphql.object(() => tester.schema.getType('FruitPurchaseOrderByFields'))
          .shouldOnlyHaveEnumKeys(['quantity', 'pricePer']);
      });
    });
  });
});
