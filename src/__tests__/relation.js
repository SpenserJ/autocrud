import { gql } from 'apollo-server-express';

import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';

const typeDefs = gql`
  type Story @autocrud(table: "stories") {
    id: ID!
    author: Author @relation(to: "storyID")
    vignettes: [Vignette!]! @relation(through: { table: "vignettes_stories", near: "storyID", far: "vignetteID" })
  }

  type Vignette @autocrud(table: "vignettes") {
    id: ID!
    tags: [VignetteTag!]! @relation(to: "vignetteID")
    stories: [Story!]! @relation(through: { table: "vignettes_stories", near: "vignetteID", far: "storyID" })
  }

  type Tag {
    id: ID!
  }

  input VignetteTagInput {
    vignetteID: ID!
    tagID: ID!
    weight: Int!
  }

  type VignetteTag @autocrud(table: "vignettes_tags", pivotInput: "VignetteTagInput") {
    vignette: Vignette! @relation(from: "vignetteID")
    vignetteID: ID!
    tag: Tag! @relation(from: "tagID")
    tagID: ID!
  }

  type Author @autocrud(table: "authors") {
    id: ID!
    storyID: ID!
    name: String!
  }
`;

describe('RelationDirective', () => {
  let tester;
  beforeAll(async () => {
    tester = await new AutoCRUDTester({
      typeDefs: [
        relationTypeDefs,
        autoCRUDTypeDefs,
        typeDefs,
      ],
      directives: {
        ...AutoCRUDDirectiveStages,
        ...RelationDirectiveStages,
      },
    });

    const { knex } = tester;

    await knex.schema.createTable('stories', (t) => {
      t.uuid('id').unique();
    });

    await knex.schema.createTable('vignettes', (t) => {
      t.uuid('id').unique();
    });

    await knex.schema.createTable('tags', (t) => {
      t.uuid('id').unique();
    });

    await knex.schema.createTable('vignettes_stories', (t) => {
      t.uuid('storyID').references('stories.id');
      t.uuid('vignetteID').references('vignettes.id');
    });

    await knex.schema.createTable('vignettes_tags', (t) => {
      t.uuid('tagID').references('tags.id');
      t.uuid('vignetteID').references('vignettes.id');
    });

    await knex.schema.createTable('authors', (t) => {
      t.uuid('id').unique();
      t.uuid('storyID').references('stories.id');
      t.string('name');
    });

    await knex('vignettes').insert([
      { id: tester.uuid('vignettes-1') },
      { id: tester.uuid('vignettes-40') },
      { id: tester.uuid('vignettes-43') },
    ]);

    await knex('stories').insert([
      { id: tester.uuid('stories-1') },
      { id: tester.uuid('stories-13') },
      { id: tester.uuid('stories-14') },
      { id: tester.uuid('stories-15') },
      { id: tester.uuid('stories-16') },
    ]);

    await knex('tags').insert([
      { id: tester.uuid('tags-30') },
      { id: tester.uuid('tags-31') },
      { id: tester.uuid('tags-32') },
      { id: tester.uuid('tags-33') },
      { id: tester.uuid('tags-34') },
    ]);

    await knex('vignettes_tags').insert([
      { vignetteID: tester.uuid('vignettes-40'), tagID: tester.uuid('tags-30') },
      { vignetteID: tester.uuid('vignettes-40'), tagID: tester.uuid('tags-31') },
      { vignetteID: tester.uuid('vignettes-1'), tagID: tester.uuid('tags-32') },
      { vignetteID: tester.uuid('vignettes-1'), tagID: tester.uuid('tags-33') },
      { vignetteID: tester.uuid('vignettes-43'), tagID: tester.uuid('tags-34') },
    ]);

    await knex('vignettes_stories').insert([
      { vignetteID: tester.uuid('vignettes-1'), storyID: tester.uuid('stories-1') },
      { vignetteID: tester.uuid('vignettes-1'), storyID: tester.uuid('stories-13') },
      { vignetteID: tester.uuid('vignettes-1'), storyID: tester.uuid('stories-14') },
    ]);

    await knex('authors').insert([
      { id: tester.uuid('authors-1'), storyID: tester.uuid('stories-1'), name: 'Alfred' },
      { id: tester.uuid('authors-2'), storyID: tester.uuid('stories-15'), name: 'Brenda' },
      { id: tester.uuid('authors-3'), storyID: tester.uuid('stories-16'), name: 'Christina' },
    ]);
  });

  test('@relation(to: ... (many))', async () => {
    const graphqlResult = await tester.query(`
      query {
        allVignettes { id tags { tagID } }
      }
    `);

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(graphqlResult).toEqual({
      data: {
        allVignettes: [
          {
            id: tester.uuid('vignettes-1'),
            tags: [
              { tagID: tester.uuid('tags-32') },
              { tagID: tester.uuid('tags-33') },
            ],
          },
          {
            id: tester.uuid('vignettes-40'),
            tags: [
              { tagID: tester.uuid('tags-30') },
              { tagID: tester.uuid('tags-31') },
            ],
          },
          {
            id: tester.uuid('vignettes-43'),
            tags: [
              { tagID: tester.uuid('tags-34') },
            ],
          },
        ],
      },
    });
  });

  test('@relation(to: ... (single))', async () => {
    const graphqlResult = await tester.query(`
      query {
        allStories { id author { name } }
      }
    `);

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(graphqlResult).toEqual({
      data: {
        allStories: [
          { id: tester.uuid('stories-1'), author: { name: 'Alfred' } },
          { id: tester.uuid('stories-13'), author: null },
          { id: tester.uuid('stories-14'), author: null },
          { id: tester.uuid('stories-15'), author: { name: 'Brenda' } },
          { id: tester.uuid('stories-16'), author: { name: 'Christina' } },
        ],
      },
    });
  });

  test('@relation(through: ...)', async () => {
    const graphqlResult = await tester.query(`
      query {
        allVignettes { id stories { id } }
      }
    `);

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(graphqlResult).toEqual({
      data: {
        allVignettes: [
          {
            id: tester.uuid('vignettes-1'),
            stories: [
              { id: tester.uuid('stories-1') },
              { id: tester.uuid('stories-13') },
              { id: tester.uuid('stories-14') },
            ],
          },
          { id: tester.uuid('vignettes-40'), stories: [] },
          { id: tester.uuid('vignettes-43'), stories: [] },
        ],
      },
    });
  });
});
