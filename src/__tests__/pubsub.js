import { gql, PubSub } from 'apollo-server-express';

import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';

const typeDefs = gql`
  type Vignette @autocrud(table: "vignettes") {
    id: ID!
    name: String!
  }

  type Query
  type Mutation
`;

const resolvers = {
  Query: {},
  Mutation: {},
};

describe('pubsub', () => {
  let tester;

  beforeEach(async () => {
    tester = await new AutoCRUDTester({
      typeDefs: [
        autoCRUDTypeDefs,
        typeDefs,
      ],
      resolvers,
      directives: { ...AutoCRUDDirectiveStages },
    });

    await tester.resetDB();

    await tester.knex.schema.createTable('vignettes', (t) => {
      t.uuid('id').unique();
      t.string('name');
    });
  });

  test('should emit an event on Create', async () => {
    const name = 'Hello, world!';

    const pubsub = new PubSub();
    const spy = jest.fn();

    pubsub.subscribe('AUTOCRUD.CREATE', spy);

    const graphqlResult = await tester.query(`
      mutation ($vignette: VignetteParams!) {
        createVignette(vignette: $vignette) {
          id
          name
        }
      }
    `, {
      vignette: {
        name,
      },
    }, {
      AutoCRUD: {
        pubsub,
      },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(spy).toHaveBeenCalledWith(expect.objectContaining({
      changes: [
        {
          operation: 'insert',
          table: 'vignettes',
          type: 'Vignette',
          id: expect.any(String),
          values: {
            id: expect.any(String),
            name,
          },
        },
      ],
      mutation: {
        name: 'createVignette',
        type: 'Vignette',
        table: 'vignettes',
      },
      changeUUID: expect.any(String),
    }));
  });

  test('should emit an event on Update', async () => {
    const id = tester.uuid('id');
    const oldName = 'Hello, world!';
    const newName = 'Hi there!';

    await tester.knex('vignettes').insert({ id, name: oldName });

    const pubsub = new PubSub();
    const spy = jest.fn();

    pubsub.subscribe('AUTOCRUD.UPDATE', spy);

    const graphqlResult = await tester.query(`
      mutation ($id: ID!, $vignette: VignetteParams!) {
        updateVignette(id: $id, vignette: $vignette) {
          id
          name
        }
      }
    `, {
      id,
      vignette: {
        name: newName,
      },
    }, {
      AutoCRUD: {
        pubsub,
      },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(spy).toHaveBeenCalledWith(expect.objectContaining({
      changes: [
        {
          operation: 'update',
          table: 'vignettes',
          type: 'Vignette',
          id,
          values: {
            name: newName,
          },
        },
      ],
      mutation: {
        name: 'updateVignette',
        type: 'Vignette',
        table: 'vignettes',
        targetID: id,
      },
      changeUUID: expect.any(String),
    }));
  });

  test('should emit an event on Delete', async () => {
    const id = tester.uuid('id');
    const name = 'Hi there!';

    await tester.knex('vignettes').insert({ id, name });

    const pubsub = new PubSub();
    const spy = jest.fn();

    pubsub.subscribe('AUTOCRUD.DELETE', spy);

    const graphqlResult = await tester.query(`
      mutation ($id: ID!) {
        deleteVignette(id: $id)
      }
    `, {
      id,
    }, {
      AutoCRUD: {
        pubsub,
      },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(spy).toHaveBeenCalledWith(expect.objectContaining({
      changes: [
        {
          id,
          operation: 'delete',
          table: 'vignettes',
          type: 'Vignette',
          values: {
            id,
            name,
          },
        },
      ],
      mutation: {
        name: 'deleteVignette',
        type: 'Vignette',
        table: 'vignettes',
        targetID: id,
      },
      changeUUID: expect.any(String),
    }));
  });

  test('should include metadata from the GraphQL context', async () => {
    const name = 'Hello, world!';
    const someMetadataObj = {};

    const pubsub = new PubSub();
    const spy = jest.fn();

    pubsub.subscribe('AUTOCRUD.CREATE', spy);

    const graphqlResult = await tester.query(`
      mutation ($vignette: VignetteParams!) {
        createVignette(vignette: $vignette) {
          name
        }
      }
    `, {
      vignette: {
        name,
      },
    }, {
      AutoCRUD: {
        pubsub,
        pubsubMetadata: {
          asyncUserData: new Promise(() => {}),
          someMetadataObj,
        },
      },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();
    expect(spy).toHaveBeenCalledWith(expect.objectContaining({
      metadata: {
        asyncUserData: expect.any(Promise),
        someMetadataObj,
      },
    }));
  });
});
