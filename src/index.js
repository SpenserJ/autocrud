// eslint-disable-next-line max-classes-per-file
import { directiveStages as RelationDirectiveStages, typeDefs as autoCRUDTypeDefs } from './RelationDirective';
import { directiveStages as AutoCRUDDirectiveStages, typeDefs as relationTypeDefs } from './AutoCRUDDirective';
import { directiveStages as DBColumnDirectiveStages, typeDefs as dbColumnTypeDefs } from './DBColumnDirective';
import getHookResolver from './hooks';
import createRequestDataLoaders from './createRequestDataLoaders';
import { applyStagedDirectives } from './utilities';

export const initialize = (schema) => {
  applyStagedDirectives(schema, {
    ...RelationDirectiveStages,
    ...AutoCRUDDirectiveStages,
    ...DBColumnDirectiveStages,
  });

  return {
    getContext: ({
      knex,
      pubsub = null,
      pubsubMetadata = null,
      hooks = null,
    }) => ({
      AutoCRUD: {
        knex,
        pubsub,
        pubsubMetadata,
        resolveHook: getHookResolver(hooks),
        ...createRequestDataLoaders(knex, schema),
      },
    }),
  };
};

export const typeDefs = [
  autoCRUDTypeDefs,
  relationTypeDefs,
  dbColumnTypeDefs,
];
