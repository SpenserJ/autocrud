import { destroyTesters } from './AutoCRUDTester';
import { destroyKnexClients } from './knex';
import './expect-graphql';

afterAll(async () => {
  await destroyTesters();
  await destroyKnexClients();
});

expect.extend({
  /**
   * Expects that the given values occur at some point in the received array,
   * in the order given, but not necessarily together. i.e.
   *
   * ```
   * const arr = [
   *   1,
   *   'apple',
   *   2,
   *   'banana',
   *   3,
   *   'cherry',
   * ]
   *
   * // Passes
   * expect(arr).toContainInOrder([
   *   1,
   *   2,
   *   (v) => v.match('che'),
   * ]);
   * ```
   *
   * @param {Array} received
   * @param {Array} expected An array of values or `find` callbacks
   */
  toContainInOrder(received, expected) {
    const found = [];
    let startIndex = 0;
    let foundAll = true;

    for (let i = 0; i < expected.length; i += 1) {
      const f = expected[i];

      const findIn = received.slice(startIndex);
      const foundIndex = findIn[(typeof f) === 'function' ? 'findIndex' : 'indexOf'](f);

      if (foundIndex > -1) {
        const realIndex = foundIndex + startIndex;
        found.push([realIndex, received[realIndex]]);

        startIndex = foundIndex + 1;
      } else {
        foundAll = false;
        break;
      }
    }

    return (foundAll)
      ? {
        message: () => `expected array not to contain the given items in order\n\nGot:\n${JSON.stringify(received, null, 2)}`,
        pass: true,
      }
      : {
        message: () => `expected array to contain the given items in order\n\nGot:\n${JSON.stringify(received, null, 2)}`,
        pass: false,
      };
  },

  array(received) {
    return (received instanceof Array || Array.isArray(received))
      ? {
        message: () => `expected not to be an array, got:\n${received}`,
        pass: true,
      }
      : {
        message: () => `expected to be an array, got:\n${received}`,
        pass: false,
      };
  },
});
