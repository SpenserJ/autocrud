/* eslint-disable max-classes-per-file */
/* eslint-disable no-restricted-syntax */
import {
  GraphQLScalarType,
  GraphQLObjectType,
  GraphQLInterfaceType,
  GraphQLUnionType,
  GraphQLEnumType,
  GraphQLInputObjectType,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';

const validGraphqlTypes = [
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLScalarType,
  GraphQLInterfaceType,
  GraphQLUnionType,
  GraphQLEnumType,
];

const baseTypes = [
  '__Directive',
  '__DirectiveLocation',
  '__EnumValue',
  '__Field',
  '__InputValue',
  '__Schema',
  '__Type',
  '__TypeKind',
  'String',
  'Boolean',
  'ID',
  'Number',
  'Float',
];

const isValidGraphQLType = (received) => validGraphqlTypes.some((t) => received instanceof t);

const listCustomTypes = (typeMap) => {
  const customTypes = Object.entries(typeMap)
    .reduce((acc, [k, v]) => {
      if (baseTypes.includes(k)) { return acc; }

      acc.push(`${k}: ${v.constructor.name}`);
      return acc;
    }, []);

  return customTypes.sort().join('\n');
};

expect.extend({
  toBeValidGraphqlType(received) {
    return {
      pass: isValidGraphQLType(received),
      message: () => (
        this.isNot
          ? `Expected ${received} not to be a valid GraphQL type, got ${received.constructor.name}`
          : `Expected ${received} to be a valid GraphQL type, got ${received.constructor.name}`
      ),
    };
  },
  graphqlObjectToHaveType(obj, type) {
    const stringifiedType = `${obj?.type}`;

    return {
      pass: stringifiedType === type,
      message: () => (
        this.isNot
          ? `Expected '${obj.name}' not to have type ${type}`
          : `Expected '${obj.name}' to have type ${type}, got ${stringifiedType}`
      ),
    };
  },
  graphqlObjectToHaveField(obj, field, type) {
    const fields = obj.getFields();
    const found = fields[field];

    const stringifiedFields = Object.values(fields).map((f) => `${f.name}: ${f.type}`);

    return {
      pass: (
        !!found
        && isValidGraphQLType(found?.type || found)
        && (type === undefined || `${found?.type}` === type)
      ),
      message: () => (
        this.isNot
          ? `Expected '${obj.name}' not to have field ${field}`
          : `Expected '${obj.name}' to have field ${field} of type ${type}, got:\n${stringifiedFields}`
      ),
    };
  },
  graphqlObjectToHaveArgument(obj, name, type) {
    const found = obj.args.find((arg) => arg.name === name);
    const stringifiedType = `${found?.type}`;

    return {
      pass: !!found && stringifiedType === type,
      message: () => (
        this.isNot
          ? `Expected '${obj.name}' not to take argument '${name}' of type '${type}'`
          : `Expected '${obj.name}' to take argument '${name}' of type '${type}', got type '${stringifiedType}'`
      ),
    };
  },
  graphqlResultNotToHaveErrors(received) {
    const errors = received.errors?.map((err) => err.extensions?.stack || err.stack) || [];

    return (!errors.length)
      ? {
        pass: true,
        message: () => 'Expected GraphQL result to have errors',
      }
      : {
        pass: false,
        message: () => `Expected GraphQL result not to have errors, got ${errors.length}:\n${errors.join('\n\n')}`,
      };
  },
  graphqlSchemaToHaveType(received, expectedType) {
    const types = received.getTypeMap();
    const found = types[expectedType];

    return {
      pass: !!found && found instanceof GraphQLObjectType,
      message: () => (
        this.isNot
          ? `Expected schema not to have type '${expectedType}'`
          : `Expected schema to have type '${expectedType}' of type GraphQLObjectType, got:\n${listCustomTypes(types)}`
      ),
    };
  },
  graphqlSchemaToHaveInput(received, expectedInput) {
    const types = received.getTypeMap();
    const found = types[expectedInput];

    return {
      pass: !!found && found instanceof GraphQLInputObjectType,
      message: () => (
        this.isNot
          ? `Expected schema not to have input '${expectedInput}'`
          : `Expected schema to have input '${expectedInput}' of type GraphQLInputObjectType, got:\n${listCustomTypes(types)}`
      ),
    };
  },
  graphqlSchemaToHaveEnum(received, expectedType) {
    const types = received.getTypeMap();
    const found = types[expectedType];

    return {
      pass: !!found && found instanceof GraphQLEnumType,
      message: () => (
        this.isNot
          ? `Expected schema not to have enum '${expectedType}'`
          : `Expected schema to have enum '${expectedType}' of type GraphQLEnumType, got:\n${listCustomTypes(types)}`
      ),
    };
  },
});

export const assertions = {
  schema: {
    shouldHaveTypes: (schema, expectedTypes) => {
      expectedTypes.forEach((type) => expect(schema).graphqlSchemaToHaveType(type));
    },
    shouldHaveInputs: (schema, expectedInputs) => {
      expectedInputs.forEach((input) => expect(schema).graphqlSchemaToHaveInput(input));
    },
    shouldHaveEnums: (schema, expectedEnums) => {
      expectedEnums.forEach((enumType) => expect(schema).graphqlSchemaToHaveEnum(enumType));
    },
    shouldHaveQueries: (schema, expectedQueries) => {
      assertions.object.shouldHaveFields(schema.getQueryType(), expectedQueries);
    },
    shouldHaveMutations: (schema, expectedMutations) => {
      assertions.object.shouldHaveFields(schema.getMutationType(), expectedMutations);
    },
  },
  object: {
    shouldHaveArguments: (field, expectedArgs) => {
      // eslint-disable-next-line no-restricted-syntax
      for (const [argName, argType] of Object.entries(expectedArgs)) {
        expect(field).graphqlObjectToHaveArgument(argName, argType);
      }
    },
    shouldOnlyHaveArguments: (field, expectedArgs) => {
      const expectedArgEntries = Object.entries(expectedArgs);
      expect(field.args.length).toEqual(expectedArgEntries.length);

      // eslint-disable-next-line no-restricted-syntax
      for (const [argName, argType] of expectedArgEntries) {
        expect(field).graphqlObjectToHaveArgument(argName, argType);
      }
    },
    shouldHaveFields: (receivedType, expectedFields) => {
      expect(receivedType).toBeValidGraphqlType();

      for (const [expectedName, [expectedType, expectedArgs]] of Object.entries(expectedFields)) {
        expect(receivedType).graphqlObjectToHaveField(expectedName, expectedType);
        const field = receivedType.getFields()[expectedName];

        if (expectedArgs) {
          assertions.object.shouldOnlyHaveArguments(field, expectedArgs);
        }
      }
    },
    shouldNotHaveFields: (receivedType, expectedFields) => {
      expect(receivedType).toBeValidGraphqlType();

      expectedFields.forEach((name) => {
        expect(receivedType).not.graphqlObjectToHaveField(name);
      });
    },
    shouldOnlyHaveFields: (receivedType, expectedFields) => {
      expect(receivedType).toBeValidGraphqlType();

      const receivedFields = Object.values(receivedType.getFields()).reduce((acc, cur) => {
        acc[cur.name] = `${cur.type}`;

        return acc;
      }, {});

      expect(receivedFields).toEqual(expectedFields);
    },
    shouldOnlyHaveEnumKeys: (receivedType, expectedNames) => {
      expect(receivedType).toBeValidGraphqlType();
      const foundNames = receivedType.getValues().map(({ name }) => name).sort();
      expect(foundNames).toEqual(expectedNames.sort());
    },
  },
};

class SchemaTester {
  constructor(schema) {
    this.schema = schema;
  }

  // Allow passing schemas as a thunk to make beforeEach, etc. testing easier
  getSchema = () => {
    if (typeof this.schema === 'function') {
      this.schema = this.schema();
    }

    return this.schema;
  }

  buildTest = (suffix, assertion) => (expected) => {
    test(`Schema should ${suffix}`, () => {
      assertions.schema[assertion](this.getSchema(), expected);
    });

    return this;
  };

  /**
   * @param {[string]} expectedTypes Expected type names
   */
  shouldHaveTypes = this.buildTest('have types', 'shouldHaveTypes');

  /**
   * @param {[string]} expectedInputs Expected input names
   */
  shouldHaveInputs = this.buildTest('have inputs', 'shouldHaveInputs');

  /**
   * @param {[string]} expectedEnums Expected enum keys
   */
  shouldHaveEnums = this.buildTest('have enums', 'shouldHaveEnums');

  /**
   * @param {{name: string, [b: string, c: string]}} expectedQueries
   * ```
   * {
   *   Name: ['[Type!]!', { ArgName: '[ArgType!]!' }]
   * }
   * ```
   */
  shouldHaveQueries = this.buildTest('have queries', 'shouldHaveQueries');

  /**
   * @param {{name: string, [b: string, c: string]}} expectedMutations
   * ```
   * {
   *   Name: ['[Type!]!', { ArgName: '[ArgType!]!' }]
   * }
   * ```
   */
  shouldHaveMutations = this.buildTest('have mutations', 'shouldHaveMutations');
}

class ObjectTester {
  constructor(obj) {
    this.obj = obj;
  }

  getObj = () => {
    if (typeof this.obj === 'function') {
      this.obj = this.obj();
    }

    return this.obj;
  }

  buildTest = (suffix, assertion) => (expected) => {
    test(`${this.obj.name} should ${suffix}`, () => {
      assertions.object[assertion](this.getObj(), expected);
    });

    return this;
  };

  shouldHaveArguments = this.buildTest('have arguments', 'shouldHaveArguments');

  shouldOnlyHaveArguments = this.buildTest('only have arguments', 'shouldOnlyHaveArguments');

  /**
   * @param {{name: string, [b: string, c: string]}} expectedFields
   * ```
   * {
   *   Name: ['[Type!]!', { ArgName: '[ArgType!]!' }]
   * }
   * ```
   */
  shouldHaveFields = this.buildTest('have fields', 'shouldHaveFields');

  /**
   * @param {[string]} expectedFields
   */
  shouldNotHaveFields = this.buildTest('not have fields', 'shouldNotHaveFields');

  shouldOnlyHaveFields = this.buildTest('only have fields', 'shouldOnlyHaveFields');

  shouldOnlyHaveEnumKeys = this.buildTest('only have enum keys', 'shouldOnlyHaveEnumKeys');
}

// eslint-disable-next-line import/prefer-default-export
export default {
  schema: (schema) => new SchemaTester(schema),
  object: (obj) => new ObjectTester(obj),
};
