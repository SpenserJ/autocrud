import { getNamedType } from 'graphql';

const noop = () => {};

export default (hooks) => async (hookPoint, hookArgs, resolverArgs) => {
  if (!hooks) { return noop; }

  const type = `${getNamedType(resolverArgs.info.returnType)}`;

  const toRun = [].concat(hooks[hookPoint], hooks[type]?.[hookPoint]).filter(Boolean);

  return toRun.reduce(
    (acc, next) => acc.then(async () => next(hookArgs, resolverArgs)),
    Promise.resolve(),
  );
};
