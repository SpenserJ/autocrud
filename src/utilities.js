// eslint-disable-next-line max-len
/* eslint no-underscore-dangle: ["error", { "allow": ["_relationInfo", "_queryInfo", "_dbColumn"] }] */
import { isListType, getNullableType, getNamedType, GraphQLScalarType } from 'graphql';
import { SchemaDirectiveVisitor } from 'apollo-server-express';

import { RelationObjectMissingError } from './errors';

// eslint-disable-next-line import/prefer-default-export
export const applyStagedDirectives = (schema, directives) => {
  const directiveStages = Object.entries(directives).reduce((acc, [name, stages]) => {
    stages.forEach((stage, i) => {
      if (!acc[i]) { acc[i] = {}; }

      acc[i][name] = stage;
    });

    return acc;
  }, []);

  directiveStages.forEach((stageDirectives) => {
    SchemaDirectiveVisitor.visitSchemaDirectives(schema, stageDirectives);
  });
};

export const pluralizeModelName = (name) => {
  if (name.endsWith('s')) { return name; }

  if (name.endsWith('y')) { return name.replace(/y$/, 'ies'); }

  return `${name}s`;
};

export const singularize = (name) => {
  if (name.endsWith('ies')) { return name.replace(/ies$/, 'y'); }

  return name.replace(/s$/, '');
};

export const camelCaseModel = (name) => `${name.slice(0, 1).toLowerCase()}${name.slice(1)}`;

export const getQueryName = (type, wantPlural = false) => {
  const finalType = getNamedType(type);

  if (wantPlural || isListType(getNullableType(type))) {
    return `all${pluralizeModelName(finalType.name)}`;
  }

  return camelCaseModel(finalType.name);
};

export const mapFieldToDBColumn = (fullType, fieldName) => {
  const model = getNamedType(fullType);
  // If we were passed a field def, use it, otherwise get the field def from the model
  const field = !model || typeof model.getFields === 'undefined'
    ? model
    : model.getFields()[fieldName];

  // If this field exists and has a dbColumn directive, return that instead of the name
  return field && field._dbColumn
    ? field._dbColumn.name
    : fieldName;
};

export const getRelationInfo = (modelProperty, throwError = true) => {
  const targetModel = getNamedType(modelProperty.type);

  if (!modelProperty._relationInfo?.from
    && !modelProperty._relationInfo?.to
    && !modelProperty._relationInfo?.through) {
    if (throwError) {
      throw new RelationObjectMissingError(modelProperty, ['from', 'to', 'through']);
    } else {
      return null;
    }
  }

  const relation = {
    from: mapFieldToDBColumn(modelProperty, modelProperty._relationInfo.from || 'id'),
    to: mapFieldToDBColumn(targetModel, modelProperty._relationInfo.to || 'id'),
    through: modelProperty._relationInfo.through,
    targetModel,
  };

  return relation;
};

export const debugObjFuncs = (toCheck) => {
  let props = [];
  let obj = toCheck;
  do {
    props = props.concat(Object.getOwnPropertyNames(obj));
  // eslint-disable-next-line no-cond-assign
  } while (obj = Object.getPrototypeOf(obj));

  return props.sort()
    .filter((e, i, arr) => (e !== arr[i + 1] && typeof toCheck[e] === 'function'));
};

export const debugObjProps = (toCheck) => {
  let props = [];
  let obj = toCheck;
  do {
    props = props.concat(Object.getOwnPropertyNames(obj));
  // eslint-disable-next-line no-cond-assign
  } while (obj = Object.getPrototypeOf(obj));

  return props.sort()
    .filter((e, i, arr) => (e !== arr[i + 1] && typeof toCheck[e] !== 'function'));
};

export const debugObj = (toCheck) => ({
  properties: debugObjProps(toCheck),
  functions: debugObjFuncs(toCheck),
});

export const isScalarField = (f) => {
  const t = getNamedType(f).type;

  return (t.ofType || t) instanceof GraphQLScalarType;
};

/**
 * Examples of recurseToRelationQueries result
 *
 * We alias each table so that we can look it up based on the field being queried
 *
 * ```
 * {
 *   // Example of a `through` relationship
 *   products_relatedProducts: knex('products as products_relatedProducts')
 *     .join(
 *       'relatedProducts as products_relatedProducts_junction',
 *       'products_relatedProducts.id',
 *       'products_relatedProducts_junction.farID',
 *     ),
 *     .where(
 *       'products.id',
 *       'products_relatedProducts_junction.nearID',
 *     ),
 *   // Example of a PivotInput relationship going all the way through
 *   customers_ratings: knex('ratings as customers_ratings')
 *     .where(
 *       'customers.id',
 *       'customers_ratings.customerID',
 *     ),
 *   customers_ratings_product: knex('products as customers_ratings_products')
 *     .where(
 *       'customers_ratings.productID',
 *       'customers_ratings_product.id',
 *     ),
 * };
 * ```
*/

export const recurseToRelationQueries = (knex, acc, paths, prevAlias = 'base') => {
  const [nextPath, ...remaining] = paths;

  if (!nextPath) { return acc; }

  const modelProperty = nextPath.onType.getFields()[nextPath.name];
  const relation = getRelationInfo(modelProperty, false);

  if (!relation) { return acc; }

  const { _implementedBy: implementedBy } = relation.targetModel;
  const queryAlias = `${prevAlias}_${nextPath.name}`;

  // If we haven't seen this relationship before, build a query for it
  if (!acc[queryAlias]) {
    acc[queryAlias] = (implementedBy || [relation.targetModel]).map((targetModel) => {
      const tableAlias = implementedBy
        ? `${queryAlias}__${targetModel}`
        : queryAlias;

      // Connect to the target model's table
      let query = knex(`${targetModel._queryInfo.table} AS ${tableAlias}`);

      // TODO: Check if it is possible to have an interface with a through
      if (relation.through) {
        // If we're going through a table, add a join and connect the where
        // between the junction table and the previous table
        query = query
          .join(
            knex.ref(`${relation.through.table} AS ${tableAlias}_junction`),
            knex.ref(`${tableAlias}.${relation.to}`),
            knex.ref(`${tableAlias}_junction.${relation.through.far}`),
          )
          .where(
            knex.ref(`${prevAlias}.${relation.from}`),
            knex.ref(`${tableAlias}_junction.${relation.through.near}`),
          );
      } else {
        // If we're not going through a junction table, just connect the two tables
        query = query.where(
          knex.ref(`${prevAlias}.${relation.from}`),
          knex.ref(`${tableAlias}.${relation.to}`),
        );
      }

      return [tableAlias, query];
    });
  }

  if (remaining.length) {
    recurseToRelationQueries(knex, acc, remaining, queryAlias);
  }

  return acc;
};

/**
 * Prints the schema's types, arguments, and fields to the console.
 *
 * @param {Object} schema A GraphQL schema
 * @param {String[]} typeNames A list of type names. If given, will only
 * print these types.
 */
export const prettyPrintSchema = (schema, typeNames = []) => {
  const out = {};

  const typeMap = schema.getTypeMap();
  const types = typeNames.length
    ? typeNames.reduce((acc, t) => { acc[t] = typeMap[t]; return acc; }, {})
    : typeMap;

  Object.values(types).forEach((type) => {
    // eslint-disable-next-line no-underscore-dangle
    if (type.name.startsWith('__') || !type._fields) { return; }

    // eslint-disable-next-line no-underscore-dangle
    out[type.name] = Object.entries(type._fields).reduce((acc, [k, v]) => {
      const args = v.args?.map(({ name: argName, type: argType }) => `${argName}: ${argType}`);

      const label = args ? `${k}(${args.join(', ')})` : k;

      acc[label] = `${v.type}`;

      return acc;
    }, {});
  });

  // eslint-disable-next-line no-console
  console.dir(out);
};
