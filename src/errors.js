/* eslint-disable max-classes-per-file */
import { getNamedType } from 'graphql';

export class CustomError extends Error {
  constructor(type, ...params) {
    super(...params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    }

    this.name = this.constructor.name;
  }
}

export class RelationObjectMissingError extends CustomError {
  constructor(field, args, ...params) {
    super(...params);
    this.field = field;
    this.args = [].concat(args);
    this.message = `Missing "${this.args.join('" or "')}" argument on "${field.name}" @relation directive`;
  }
}

export class RelationAllQueryMissingError extends CustomError {
  constructor(field, queryName, info, ...params) {
    super(...params);
    this.field = field;

    const parentType = info.parentType.name;
    const namedType = getNamedType(field.type);
    this.message = `Unable to resolve relation from type '${parentType}' to type '${namedType}'\nMissing query '${queryName}' - is the @autocrud directive configured on '${namedType}'?`;
  }
}

export class GraphQLUnsupportedFilterOperation extends CustomError {
  constructor(type, property, operation, ...params) {
    super(...params);
    this.message = `Cannot apply "${operation}" filter to ${type}.${property}`;
    this.type = type;
    this.property = property;
    this.operation = operation;
  }
}

export class MissingDataLoaderContext extends CustomError {
  constructor(...params) {
    super(...params);
    this.message = 'Missing AutoCRUD data loaders in query context. Please ensure @autocrud directive is properly configured';
  }
}

export class MissingIDField extends CustomError {
  constructor(field, ...params) {
    super(...params);
    this.message = `Missing 'id' field for type '${field.name}'. This field is required for AutoCRUD to function.`;
    this.field = field;
  }
}

export class MissingTable extends CustomError {
  constructor(field, ...params) {
    super(...params);
    this.message = `Missing 'table' in @autocrud arguments for type ${field.name}`;
    this.field = field;
  }
}

export class MissingQueriesDirective extends CustomError {
  constructor(field, ...params) {
    super(...params);
    this.message = `Missing @autocrud directive on definition for "${field.name}" type`;
    this.field = field;
  }
}

export class MissingRelationDirective extends CustomError {
  constructor(model, field, ...params) {
    super(...params);
    this.message = `Missing @relation directive on ${model.name}.${field.name}`;
    this.model = model;
    this.field = field;
  }
}

export class DeleteRecordFailure extends CustomError {
  constructor(type, id, ...params) {
    super(...params);
    this.message = `Could not find ${type} with id "${id}" to delete`;
    this.type = type;
    this.id = id;
  }
}

export class CreateUpdateNeedsRead extends CustomError {
  constructor(type, ...params) {
    super(...params);
    this.message = `Cannot use create or update mutations without readOne query on ${type}`;
    this.type = type;
  }
}

export class DuplicateMutationInputType extends CustomError {
  constructor(name, ...params) {
    super(...params);
    this.message = `Mutation ${name} already exists in schema`;
    this.name = name;
  }
}

export class FieldParsingError extends CustomError {
  constructor(field, ...params) {
    super(...params);
    this.message = `Could not parse field '${field}' during mutation. If this field is only meant to be in your GraphQL schema, but not the database, use a preCommit hook to transform the data into the expected shape.`;
    this.field = field;
  }
}

export class UnexpectedOperationDuringUpdate extends CustomError {
  constructor(operation, found, ...params) {
    super(...params);
    this.message = `Unexpected ${operation} when updating base models:\n${JSON.stringify(found, null, 2)}`;
    this.operation = operation;
  }
}

export class MissingPivotInputType extends CustomError {
  constructor(inputName, ...params) {
    super(...params);
    this.message = `Could not find pivot input type named "${inputName}"`;
    this.inputName = inputName;
  }
}

export class MissingPivotKey extends CustomError {
  constructor(table, key, ...params) {
    super(...params);
    this.message = `Missing pivot key ${key} for mutation on table ${table}`;
    this.table = table;
    this.key = key;
  }
}

export class UnexpectedIDsOnPivot extends CustomError {
  constructor(model, keys, ...params) {
    super(...params);
    this.message = `Expected 2 ID fields on ${model.name} type, but found ${JSON.stringify(keys)}`;
    this.model = model;
    this.keys = keys;
  }
}

export class PivotInputMismatchedFields extends CustomError {
  constructor(input, pivot, extraKeys, ...params) {
    super(...params);
    this.message = `Found extra keys when converting from ${input.name} input type to ${pivot.name} model: ${JSON.stringify(extraKeys)}`;
    this.input = input;
    this.pivot = pivot;
    this.extraKeys = extraKeys;
  }
}

export class UnknownRelationType extends CustomError {
  constructor(model, field, ...params) {
    super(...params);
    this.message = `Unknown relation type for ${model.name}.${field.name}`;
    this.model = model;
    this.field = field;
  }
}

export class MissingTypeResolver extends CustomError {
  constructor(field, ...params) {
    super(...params);
    this.message = `Interface '${field.name}' does not have a type resolver. Add the following to your schema:\nconst resolvers = {\n  ${field.name}: { __resolveType: (obj) => obj.autocrud_model }\n};`;
    this.field = field;
  }
}

export class DBColumnIDError extends CustomError {
  constructor(type, field, ...params) {
    super(...params);
    this.message = `Field 'id' on type '${type.name}' cannot use the @dbColumn directive. 'id' is required for AutoCRUD to function.`;
    this.type = type;
    this.field = field;
  }
}
