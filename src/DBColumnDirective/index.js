// eslint-disable-next-line max-len
/* eslint no-underscore-dangle: ["error", { "allow": ["_dbColumn"] }] */
/* eslint-disable max-classes-per-file */

import { SchemaDirectiveVisitor, gql } from 'apollo-server-express';
import { DBColumnIDError } from '../errors';

class DBColumnDirectiveStage1 extends SchemaDirectiveVisitor {
  visitFieldDefinition(field, details) {
    if (field.name === 'id') { throw new DBColumnIDError(details.objectType, field); }

    field._dbColumn = this.args; // eslint-disable-line no-param-reassign

    // Don't override existing resolvers
    if (field.resolve) { return; }

    /* eslint-disable no-param-reassign */
    field.resolve = async (obj) => obj[field._dbColumn.name];
    // Indicate that this resolve function was added by AutoCRUD, and is not a
    // custom resolver
    field.resolve.autocrudInternal = true;
    /* eslint-enable no-param-reassign */
  }
}

export const directiveStages = {
  dbColumn: [
    DBColumnDirectiveStage1,
  ],
};

export const typeDefs = gql`
  directive @dbColumn(
    name: String!,
  ) on FIELD_DEFINITION
`;
